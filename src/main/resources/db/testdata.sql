-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: routegrader
-- ------------------------------------------------------
-- Server version       5.5.41-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment` varchar(255) DEFAULT NULL,
  `route` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ektg0a1h57bv6nhuyjygxon0u` (`route`),
  CONSTRAINT `FK_ektg0a1h57bv6nhuyjygxon0u` FOREIGN KEY (`route`) REFERENCES `route` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grade_entry`
--

DROP TABLE IF EXISTS `grade_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grade_entry` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grade` varchar(255) DEFAULT NULL,
  `route` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_94dnn9291o3blfp01dmudow1u` (`route`),
  CONSTRAINT `FK_94dnn9291o3blfp01dmudow1u` FOREIGN KEY (`route`) REFERENCES `route` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table `grade_entry`
--

LOCK TABLES `grade_entry` WRITE;
/*!40000 ALTER TABLE `grade_entry` DISABLE KEYS */;
INSERT INTO `grade_entry` VALUES (1,'FIVE',1),(2,'FIVE',1),(3,'FIVE',1),(4,'FIVE',1),(5,'FIVE',1),(6,'FIVE',1),(7,'FIVE_MINUS',1),(8,'FIVE_MINUS',1),(9,'FIVE_MINUS',1),(10,'SIX_MINUS',1),(11,'FIVE_PLUS',1),(12,'FIVE_PLUS',1),(13,'FIVE_PLUS',1),(14,'FIVE_PLUS',1),(15,'FIVE_PLUS',1),(16,'FIVE_PLUS',1),(17,'SEVEN_PLUS',1),(18,'FIVE',1),(19,'FIVE_PLUS',2),(20,'SIX',2),(21,'SIX_MINUS',2),(22,'FIVE',2),(23,'FIVE',1),(24,'FIVE',1),(25,'FIVE',1),(26,'FIVE',1),(27,'SIX',1),(28,'SIX',1),(29,'SIX',1),(30,'SIX',2);
/*!40000 ALTER TABLE `grade_entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route`
--

DROP TABLE IF EXISTS `route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `routesetter` varchar(255) NOT NULL,
  `wall` int(11) NOT NULL,
  `built` date DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_aabj5o4kmy45i6yd0c46l4cgu` (`routesetter`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route`
--

LOCK TABLES `route` WRITE;
/*!40000 ALTER TABLE `route` DISABLE KEYS */;
INSERT INTO `route` VALUES (1,'Bästa leden','Pär Nilsson',13,'2015-03-14','blå'),(2,'leden2','Tim Testsson',1,'2015-02-27','röd'),(3,'led3','Jim Testsson',2,'2015-01-01','svart');
/*!40000 ALTER TABLE `route` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-21 22:23:04
