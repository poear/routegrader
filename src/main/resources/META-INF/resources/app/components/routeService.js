angular.module('routeGrader.routeService', ['routeGrader.routeResource']).factory('RouteService', ['Route', function (Route) {
    return {
        query: function () {
            return Route.get().$promise.then(function (result) {
                if (result._embedded !== undefined){
                    return result._embedded.routes;
                }
                return [];
            });
        },
        get: function (id) {
            return Route.get({id: id}).$promise.then(function (result) {
                return result;
            });
        },
        save: function (route) {
            return Route.save(route).$promise.then(function (result) {
                return result;
            });
        }
    }
}]);