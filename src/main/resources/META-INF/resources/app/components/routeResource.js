angular.module('routeGrader.routeResource', ['ngResource']).factory('Route', ['$resource', function ($resource) {
    return $resource('/api/routes/:id');
}]);