angular.module('routeGrader.routeGradesResource', ['ngResource']).factory('RouteGrades', ['$resource', function ($resource) {
    return $resource('/api/routes/:id/gradeEntry');
}]);

angular.module('routeGrader.gradeResource', ['ngResource']).factory('Grade', ['$resource', function ($resource) {
    return $resource('/api/grades');
}]);
