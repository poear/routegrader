angular.module('routeGrader.gradeService', ['routeGrader.routeGradesResource', 'routeGrader.gradeResource']).factory('GradeService', ['RouteGrades', 'Grade', function (RouteGrades, Grade) {
    return {
        get: function (id) {
            return RouteGrades.get({id: id}).$promise.then(function (result) {
                var chartData = {labels: [], series: []};

                if (result._embedded === undefined) {
                    return chartData;
                }

                var grades = result._embedded.grades;
                var countedVotes = {};

                for (var i = grades.length - 1; i >= 0; i--) {
                    var key = grades[i].grade.label;
                    countedVotes[key] = countedVotes[key] === undefined ? 1 : countedVotes[key] + 1;
                }

                for (var key in countedVotes) {
                    if (countedVotes.hasOwnProperty(key)) {
                        chartData.labels.push(key);
                        chartData.series.push(countedVotes[key]);
                    }
                }

                return chartData;
            });
        },
        save: function (gradeEntry) {
            return Grade.save(gradeEntry);
        }
    }
}]);