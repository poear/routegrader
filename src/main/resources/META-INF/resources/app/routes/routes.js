'use strict';

angular.module('routeGrader.routes', ['ngRoute', 'ngTouch', 'routeGrader.routeService'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/routes', {
            templateUrl: 'app/routes/routes.html',
            controller: 'RoutesCtrl',
            resolve: {
                routes: ['RouteService', function (RouteService) {
                    return RouteService.query();
                }]
            }
        });
    }])

    .controller('RoutesCtrl', ['$scope', '$location', 'routes', 'RouteService', function ($scope, $location, routes, RouteService) {

        var defaultForm = {built: new Date()};

        $scope.routes = routes;
        $scope.route = angular.copy(defaultForm);
        $scope.formActive = false;

        $scope.toggleFormActivity = function () {
            $scope.formActive = !$scope.formActive;
        };

        $scope.addRoute = function () {
            if ($scope.newRoute.$valid) {
                RouteService.save($scope.route).then(function (result) {
                    RouteService.query().then(function (result) {
                        $scope.routes = result;
                        $scope.formActive = false;
                        $scope.newRoute.$setPristine();
                        $scope.route = angular.copy(defaultForm);
                    });
                });
            }
            ;
        };

        $scope.viewDetails = function (routeId) {
            $location.path('/route/' + routeId);
        };
    }]);