'use strict';

// Declare app level module which depends on views, and components
angular.module('routeGrader', [
    'ngRoute',
    'routeGrader.routes',
    'routeGrader.route'
]).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/routes'});
    }]);
