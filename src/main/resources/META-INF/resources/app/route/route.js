'use strict';

angular.module('routeGrader.route', ['ngRoute', 'ngStorage', 'ngTouch', 'routeGrader.routeService', 'routeGrader.gradeService'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/route/:id', {
            templateUrl: 'app/route/route.html',
            controller: 'RouteCtrl',
            resolve: {
                route: ['RouteService', '$route', function (RouteService, $route) {
                    return RouteService.get($route.current.params.id);
                }],
                grades: function () {
                    return [
                        {label: "4", value: "FOUR"},
                        {label: "5-", value: "FIVE_MINUS"},
                        {label: "5", value: "FIVE"},
                        {label: "5+", value: "FIVE_PLUS"},
                        {label: "6-", value: "SIX_MINUS"},
                        {label: "6", value: "SIX"},
                        {label: "6+", value: "SIX_PLUS"},
                        {label: "7-", value: "SEVEN_MINUS"},
                        {label: "7", value: "SEVEN"},
                        {label: "7+", value: "SEVEN_PLUS"}
                    ];

                },
                chartData: ['GradeService', '$route', function (GradeService, $route) {
                    return GradeService.get($route.current.params.id);
                }]
            }
        });
    }])

    .controller('RouteCtrl', ['$scope', '$routeParams', '$localStorage', '$route', 'route', 'grades', 'chartData', 'GradeService', function ($scope, $routeParams, $localStorage, $route, route, grades, chartData, GradeService) {
        if ($localStorage.gradedRoutes === undefined || $localStorage.gradedRoutes === null) {
            $localStorage.gradedRoutes = {};
        }

        $scope.grade = null;
        $scope.grades = grades;

        $scope.route = route;
        $scope.route.id = $routeParams.id;

        $scope.submitVote = function () {
            GradeService.save({grade: $scope.grade.value, route: 'routes/' + $scope.route.id}).$promise.then(
                function () {
                    GradeService.get($route.current.params.id).then(function (chartData) {
                        new Chartist.Pie('.ct-chart', chartData, {
                            chartPadding: 30,
                            labelOffset: 50,
                            labelDirection: 'explode'
                        });

                        $localStorage.gradedRoutes[$scope.route.id] = true;
                    });
                }
            );
        };

        $scope.back = function () {
            window.history.back();
        };

        $scope.$watch(function () {
            return $localStorage.gradedRoutes[$scope.route.id] === true
        }, function (newVal, oldVal, scope) {
            $scope.chartData = GradeService.get($route.current.params.id);
            scope.routeGraded = newVal;
        });

        new Chartist.Pie('.ct-chart', chartData, {
            chartPadding: 30,
            labelOffset: 50,
            labelDirection: 'explode'
        });

    }]);