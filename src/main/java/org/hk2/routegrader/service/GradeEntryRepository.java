package org.hk2.routegrader.service;

import org.hk2.routegrader.domain.GradeEntry;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Set;


@RepositoryRestResource(collectionResourceRel = "grades", path = "grades")
public interface GradeEntryRepository extends Repository<GradeEntry, Long> {

    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed the
     * entity instance completely.
     *
     * @return the saved entity
     */
    GradeEntry save(GradeEntry entity);


    /**
     * Returns all instances of the type.
     *
     * @return all entities
     */
    Set<GradeEntry> findAll();
}
