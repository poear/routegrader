package org.hk2.routegrader.service;

import org.hk2.routegrader.domain.Route;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Set;


@RepositoryRestResource(collectionResourceRel = "routes", path = "routes")
public interface RouteRepository extends Repository<Route, Long> {

    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed the
     * entity instance completely.
     *
     * @return the saved entity
     */
    Route save(Route entity);


    /**
     * Retrieves an entity by its id.
     *
     * @param id must not be {@literal null}.
     * @return the entity with the given id or {@literal null} if none found
     * @throws IllegalArgumentException if {@code id} is {@literal null}
     */
    Route findOne(Long id);


    /**
     * Returns all instances of the type.
     *
     * @return all entities
     */
    Set<Route> findAll();

}
