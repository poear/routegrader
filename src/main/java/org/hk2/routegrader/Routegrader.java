package org.hk2.routegrader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Routegrader {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Routegrader.class, args);
    }

}
