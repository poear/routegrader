package org.hk2.routegrader.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Immutable
public class GradeEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "route", nullable = false)
    private Route route;

    @Enumerated(EnumType.STRING)
    private ScandinavianGrade grade;

}
