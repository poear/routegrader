package org.hk2.routegrader.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
public enum ScandinavianGrade {

    FOUR("4"),
    FIVE_MINUS("5-"),
    FIVE("5"),
    FIVE_PLUS("5+"),
    SIX_MINUS("6-"),
    SIX("6"),
    SIX_PLUS("6+"),
    SEVEN_MINUS("7-"),
    SEVEN("7"),
    SEVEN_PLUS("7+");

    private final String label;

    ScandinavianGrade(String label) {
        this.label = label;
    }

    @JsonProperty
    public String getValue(){
        return this.name();
    }

}
