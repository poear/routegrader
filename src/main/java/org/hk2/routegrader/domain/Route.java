package org.hk2.routegrader.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@Immutable
public class Route implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String routesetter;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private Integer wall;

    @Temporal(TemporalType.DATE)
    @Column
    private Date built;

    @Column
    private String color;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "route")
    private List<GradeEntry> gradeEntry;

    @JsonProperty("routeId")
    public Long getRouteId() {
        return this.id;
    }

    public Date getBuilt() {
        if (this.built == null) {
            return null;
        } else {
            return (Date) this.built.clone();
        }
    }

    public void setBuilt(Date built) {
        if (built == null) {
            this.built = null;
        } else {
            this.built = (Date) built.clone();
        }
    }
}
