### Routegrader ###

* An app that simplify the grading process of new routes in climbing-gyms.
* v 0.2

### Running Routegrader ###
Prerequisites for running Routegrader:

 * Java runtime environment (JRE) of version 7 or higher.

From commandline:

    java -jar routegrader.jar

Default properties are:

    server.port=8080
    spring.data.rest.baseUri=/api
    spring.datasource.url=jdbc:mysql://localhost/routegrader
    spring.datasource.username=routegrader
    spring.datasource.password=routegrader
    spring.datasource.driver-class-name=com.mysql.jdbc.Driver
    spring.datasource.testOnBorrow=true
    spring.datasource.validationQuery=SELECT 1

    server.tomcat.remote_ip_header=x-forwarded-for
    server.tomcat.protocol_header=x-forwarded-proto

    spring.jpa.database-platform=org.hibernate.dialect.MySQL5InnoDBDialect
    spring.jpa.show-sql=false
    spring.jpa.hibernate.ddl-auto=none

Any of the above properties _can_ be overridden by adding an argument to the startup of Routegrader.

To initialize the database tables you could start the app with '--spring.jpa.hibernate.ddl-auto=create' eg.
    java -jar routegrader.jar --spring.jpa.hibernate.ddl-auto=create
    
__Do not__ override `spring.data.rest.baseUri` the angular resources are dependent on this property to be set to it's default value of "/api"

To run the server on port 80 instead, simply add the following argument

    java -jar routegrader.jar --server.port=80

Other spring boot properties can be found [here](http://docs.spring.io/spring-boot/docs/1.2.3.RELEASE/reference/html/common-application-properties.html "Spring Boot reference doc")


### How do I get set up? ###

  + You need will need:
    * Java Dev. kit (recommend JDK 1.7 or higher)
    * Maven
  + Configuration
    * To run the app you do not need to configure anything. However, if you want to change anything do so in the application.properties file. Have a look at [Common Spring Boot properties](http://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/htmlsingle/#appendix)
  + Database configuration
    * By default a db will be run in memory, thus not persisted on restart of application.
  + Deployment instructions (from project root)
    * mvn clean install
    * java -jar target/routegrader-xx.jar
    * If everything worked as expected you find the app running on [http://localhost:8080/](http://localhost:8080/)

### Some screenshots ###

![image2.PNG](https://bitbucket.org/repo/LxRpEb/images/290584202-image2.PNG)
![image3.PNG](https://bitbucket.org/repo/LxRpEb/images/2036836284-image3.PNG)
![image1.PNG](https://bitbucket.org/repo/LxRpEb/images/2734391798-image1.PNG)